function defaults(obj, defaultProps) {
    for (let d in defaultProps){
        if(d in obj && obj[d]==undefined){
            obj[d]=defaultProps[d];
        }
    }
    return obj;
}
module.exports=defaults;
