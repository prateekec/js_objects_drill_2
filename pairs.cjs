function pairs(obj) {
    let ans=[];
    for(let o in obj){
        ans.push([o,obj[o]]);
    }
    return ans;
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
}
module.exports=pairs;