function mapObject(obj, cb) {
    for(let o in obj){
        obj[o]=cb(obj[o]);
    }
    return obj;
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
}
module.exports=mapObject;